from django.test import TestCase,Client
from django.urls import resolve
from .views import index,submit
from .models import AddStatus
from .forms import MyForm
from Story7.urls import redirHome
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story7UnitTest(TestCase):

	def test_story_7_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)
    
	def test_story_7_using_redirHome_func(self):
		found = resolve('/')
		self.assertEqual(found.func, redirHome)

	def test_story_7_confirmation_page_url_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)
    
	def test_model_can_create_new_status(self):
	    # Creating a new activity
	    new_activity = AddStatus.objects.create(name='test', status='status test')
	
	    # Retrieving all available activity
	    counting_all_available_status = AddStatus.objects.all().count()
	    self.assertEqual(counting_all_available_status, 1)

	def test_landing_page(self):
		response = self.client.get('/home/')
		self.assertTemplateUsed(response, 'Story7.html')

	def test_form_validation_for_blank_name(self):
		form = MyForm(data={'name': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['name'],["This field is required."])
    
	def test_form_validation_for_blank_field(self):
		form = MyForm(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'],["This field is required."])
		
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('chromedriver', chrome_options=chrome_options)
		super(Story7UnitTest, self).setUp()

	def test_input_new_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		Name = selenium.find_element_by_name('name')
		Status = selenium.find_element_by_name('status')
		Submit = selenium.find_element_by_id('submit')

		Name.send_keys('ftest')
		Status.send_keys('ftest')
		Submit.send_keys(Keys.RETURN)

		time.sleep(2)

		yes = selenium.find_element_by_id('buttonyes')
		yes.send_keys(Keys.RETURN)

		time.sleep(2)
		page = selenium.page_source
		self.assertIn('ftest', page)

	def test_input_new_status_but_cancelled(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		Name = selenium.find_element_by_name('name')
		Status = selenium.find_element_by_name('status')
		Submit = selenium.find_element_by_id('submit')

		Name.send_keys('no thanks')
		Status.send_keys('no test')
		Submit.send_keys(Keys.RETURN)

		time.sleep(2)

		no= selenium.find_element_by_id('buttonno')
		no.click()

		time.sleep(2)
		page = selenium.page_source
		self.assertNotIn('no thanks', page)

	def tearDown(self):
		self.selenium.quit()
		super(Story7UnitTest, self).tearDown()