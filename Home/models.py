from django.db import models

class AddStatus(models.Model):
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=150)
    date=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % self.name
