from django.forms import ModelForm
from .models import AddStatus

class MyForm(ModelForm):
    class Meta:
        model=AddStatus
        fields='__all__'