from django.shortcuts import render, redirect
from .forms import MyForm
from .models import AddStatus
from django.http import HttpResponseBadRequest

def index(request):
    if request.method == 'POST':
        #We use POST just for validation
        form = MyForm(request.POST)
        if form.is_valid():
            form_data = {'name': request.POST.get('name'), 'status': request.POST.get('status')}
            return render(request, 'Submission.html', form_data)

    #If the request is not a post or form is not valid, we render entries
    YourStatus = AddStatus.objects.all().order_by('-date')
    return render(request, 'Story7.html', {'YourStatus': YourStatus})

def submit(request):
    """POST end-point for savind data"""

    form = MyForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('/home')

    else:
        #The index page validates the form, so this should never happen
        return HttpResponseBadRequest('Invalid or incomplete fields')